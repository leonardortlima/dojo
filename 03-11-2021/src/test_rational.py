from unittest import TestCase

from rational import Rational

class RationalTest(TestCase):

    def test_create_with_float(self):
        #@todo throw exception
        half = Rational(0.5)

    def test_str(self):
        half = Rational(1, 2)
        self.assertEqual(str(half), "1/2")

    def test_str_one_parameter(self):
        two = Rational(2)
        self.assertEqual(str(two), "2/1")

    def test_multiply(self):
        two = Rational(2)
        four = two * two
        self.assertEqual(str(four), "4/1")
        eight = four * two
        self.assertEqual(str(eight), "8/1")
        sixteen = eight * 2
        self.assertEqual(str(sixteen), "16/1")

    def test_div(self):
        two = Rational(2)
        one = two / two
        self.assertEqual(str(one), "2/2")
        four = Rational(4)
        two = four / two
        self.assertEqual(str(two), '4/2')
        two = four / 2
        self.assertEqual(str(two), '4/2')

    def test_sum(self):
        two = Rational(2)
        four = two + two
        self.assertEqual(str(four), '4/1')
        one = Rational(2, 2)
        five = four + one
        self.assertEqual(str(five), "10/2")
        five = four + 1
        self.assertEqual(str(five), "5/1")
        two = one + 1
        self.assertEqual(str(two), "4/2")

    def test_sub(self):
        two = Rational(2)
        zero = two - two
        self.assertEqual(str(zero), "0/1")
        zero = two - 2
        self.assertEqual(str(zero), "0/1")

    def test_compare(self):
        self.assertNotEqual(Rational(2), object())
        self.assertEqual(Rational(2, 1), Rational(4, 2))
        self.assertNotEqual(Rational(3, 1), Rational(4, 2))
        self.assertEqual(Rational(2, 1), Rational(2, 1))
        self.assertEqual(2, Rational(2, 1))

    def test_to_float(self):
        self.assertEqual(float(Rational(2, 1)), 2.)
        self.assertEqual(float(Rational(2, 2)), 1.)
        self.assertAlmostEqual(float(Rational(8, 3)), 2.6666666666, places=5)