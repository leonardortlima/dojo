
const fs = require('fs');

const splitColumns = (linha) =>
    linha.split(/\s+/g).filter(it => it != '');

const splitLines = (string) => string.split(/[\n]+\s*/m).filter(it => it != '');

const readMatriz = (string) => splitLines(string).map(splitColumns);

const readFileAsMatriz =
    (file, encoding, fn) =>
         fs.readFile( file, encoding, (error, data) => fn(error, readMatriz(data)));


module.exports = {
    splitColumns,
    splitLines,
    readMatriz,
    readFileAsMatriz
};