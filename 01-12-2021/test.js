const fs = require('fs');
const { splitColumns, splitLines, readMatriz, readFileAsMatriz } = require('./');

test('adds 1 + 2 to equal 3', () => {
    expect( 1 + 2).toBe(3);
});

test('teste exploratorio do fs', done => {
    fs.readFile('test.txt', 'utf8', function(err, data) {
        if (err) {
            done(err);
        }
        expect(data).toBe('conteudo');
        done();
    });
});

test('teste exploratorio do fs com quebra de linha', done => {
    fs.readFile('multiple_lines.txt', 'utf8', function(err, data) {
        if (err) {
            done(err);
        }
        expect(data).toBe('line 1\nline 2');
        done();
    });
});


test('separar colunas', () => {
    const linha = "  Dy MxT   MnT";
    const columns = splitColumns(linha);
    expect(columns).toStrictEqual(["Dy","MxT","MnT"]);
});

test("separar colunas com outra string", () => {
  const linha = "  a b   c";
  const columns = splitColumns(linha);
  expect(columns).toStrictEqual(['a', 'b', 'c']);
});

test("separar linhas em outro array", () => {
    const string = 'line 1\nline 2';
    const lines = splitLines(string);
    expect(lines).toStrictEqual(['line 1', 'line 2']);
});

test("separar linhas em outro outro array", () => {
  const string = 'line 3\n\n\nline 4\nline7\n\nline8\n \n';
  const lines = splitLines(string);
  expect(lines).toStrictEqual(['line 3', 'line 4', 'line7', 'line8']);
});

test("ler matriz", () => {
    const string = [
        ["a", "b", "c"].join(" "),
        ["1", "2", "3"].join(" "),
    ].join("\n");
    const matriz = readMatriz(string);
    expect(matriz).toStrictEqual([
        ["a", "b", "c"],
        ["1", "2", "3"],
    ]);
});


test("ler matriz cavala", () => {
    const string = [
        ["a", "b", "c", "g", "l"].join(" "),
        ["1", "2"].join(" "),
        ["1", "2", "3"].join(" "),
    ].join("\n");
    const matriz = readMatriz(string);
    expect(matriz).toStrictEqual([
        ["a", "b", "c", "g", "l"],
        ["1", "2"],
        ["1", "2", "3"]
    ]);
});

test("ler o arquivo multiple_line como matriz ", done => {
    readFileAsMatriz('multiple_lines.txt', 'utf8', function(err, matriz) {
        if (err) {
            done(err);
        }

        expect(matriz).toStrictEqual( [ [ 'line', '1' ], [ 'line', '2' ] ]);
        done();
    });
});

test("ler o arquivo weather.dat como matriz", done => {
    readFileAsMatriz('weather.dat', 'utf8', function(err, matriz) {
        if (err) {
            done(err);
        }

        console.log(matriz);
        expect(matriz.length).toBe(32);
        expect(matriz[0].length).toStrictEqual(17);
        done();
    });
})