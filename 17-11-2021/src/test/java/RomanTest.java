import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat; 

public class RomanTest {

    private NumberToString numberToString = new NumberToString.Roman();

    @Test
    public void testOne() {
        String result = numberToString.convert(1);
        assertThat(result)
            .isEqualTo("I");
    }

    @Test
    public void testThree() {
        String result = numberToString.convert(3);
        assertThat(result)
            .isEqualTo("III");
    }

    @Test
    public void testFour() {
        String result = numberToString.convert(4);
        assertThat(result)
            .isEqualTo("IV");
    }

    @Test
    public void testFive() {
        String result = numberToString.convert(5);
        assertThat(result)
            .isEqualTo("V");
    }

    @Test
    public void testEight() {
        String result = numberToString.convert(8);
        assertThat(result)
            .isEqualTo("VIII");
    }

    @Test
    public void testNine() {
        String result = numberToString.convert(9);
        assertThat(result)
            .isEqualTo("IX");
    }

    @Test
    public void testTen() {
        String result = numberToString.convert(10);
        assertThat(result)
            .isEqualTo("X");
    }

    @Test
    public void testTwenty() {
        String result = numberToString.convert(20);
        assertThat(result)
            .isEqualTo("XX");
    }


    @Test
    public void testFifty() {
        String result = numberToString.convert(50);
        assertThat(result)
            .isEqualTo("L");
    }

    @Test
    public void testOneHundred() {
        String result = numberToString.convert(100);
        assertThat(result)
            .isEqualTo("C");
    }

    @Test
    public void testFiveHundred() {
        String result = numberToString.convert(500);
        assertThat(result)
            .isEqualTo("D");
    }

    @Test
    public void testOneThousand() {
        String result = numberToString.convert(1000);
        assertThat(result)
            .isEqualTo("M");
    }

    @Test
    public void test2021() {
        String result = numberToString.convert(2021);
        assertThat(result)
            .isEqualTo("MMXXI");
    }

    @Test
    public void test999() {
        String result = numberToString.convert(999);
        assertThat(result)
            .isEqualTo("CMXCIX");
    }

    @Test
    public void test364() {
        String result = numberToString.convert(364);
        assertThat(result)
            .isEqualTo("CCCLXIV");
    }
}