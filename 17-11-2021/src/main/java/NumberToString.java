import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

public interface NumberToString {

    public String convert(long number);

    public class Roman implements NumberToString {

        private Map<Long, String> symbols = new HashMap();

        public Roman() {
            symbols.put(1L, "I");
            symbols.put(4L, "IV");
            symbols.put(5L, "V");
            symbols.put(9L, "IX");
            symbols.put(10L, "X");
            symbols.put(40L, "XL");
            symbols.put(50L, "L");
            symbols.put(90L, "XC");
            symbols.put(100L, "C");
            symbols.put(400L, "CD");
            symbols.put(500L, "D");
            symbols.put(900L, "CM");
            symbols.put(1000L, "M");
        }

        public String convert(long number) {
            String result = "";
            while(number > 0) {
                long value = greaterKeyLowestThan(number);
                result += symbols.get(value);
                number -= value;
            }
            return result;
        }

        private long greaterKeyLowestThan(long number) {
            return symbols
                .keySet()
                .stream()
                .sorted(Collections.reverseOrder())
                .filter( it -> it <= number)
                .findFirst()
                .get();
        }
    }

}